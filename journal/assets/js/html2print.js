// html2print needs CSS regions
// load a ‘polyfill’ if the browser does not support it
if (window.chrome) {
    console.log('running chrome, no support for css regions; loading the polyfill');
    var script = document.createElement('script');
    script.setAttribute('src', 'assets/lib/css-regions.min.js');
    document.getElementsByTagName('head')[0].appendChild(script);
};



jQuery(document).ready(function($) {
    console.log("launching html2print");

    // ________________________________ INIT __________________________________ //
    //$("body").wrapInner("<div id='h2p-content'></div>");

    // INSERTING THE PAGES STRUCTURE
    window.setTimeout(function(){


        $("body").prepend(page_structure).ready(function(){
            // Calculating the number of pages needed
            var content_height = $("body").height();
            var body_height = $($("#h2p-master-page .h2p-body")[0]).height();
            var nb_page = Math.ceil(content_height / body_height) + 20;

            // Cloning the master page
            //for (i = 1; i <= nb_page; i++){
            //    $("#h2p-master-page").clone().attr("id","h2p-page-"+i).insertBefore($("#h2p-master-page"));
            //}
            
            //$("#h2p-master-page").attr("data-width", $(".h2p-paper:first-child").width()).hide();
            //$("#h2p-master-page .h2p-recipient").removeClass("h2p-recipient");
            $("body").css("width", "100%");
            $("html").css("width", "100%");

            $(".h2p-paper").append("<div class='crops'><div class='crop-top-left'><span class='bleed--horizontal'></span><span class='bleed--vertical'></span></div><div class='crop-top-right'><span class='bleed--horizontal'></span><span class='bleed--vertical'></span></div><div class='crop-bottom-right'><span class='bleed--horizontal'></span><span class='bleed--vertical'></span></div><div class='crop-bottom-left'><span class='bleed--horizontal'></span><span class='bleed--vertical'></span></div></div>")
        });
    }, 1000);

});

