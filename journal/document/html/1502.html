<div id="texteNicolas">
	<h1>Mais<br>c’est<br>quoi<br>la<br>magie<br>?</h1>
	<h2>Nicolas Malevé</h2>
	
	<p>
		« Mais c’est quoi la magie? » La question résonne après une heure de discussion. Nous sommes six autour de la table : Alex, Stéphanie, leur fille Elli, Sarah, Gijs et moi. Alex, Stéphanie, Sarah et Gijs sont membres du collectif Open Source Publishing mieux connu sous le diminutif OSP.  Je suis les aventures d’OSP depuis sa création en 2006. Lorsque Sarah et Alex m'ont demandé d'écrire un texte accompagnant leur exposition, j'ai saisi l'occasion d'aller plus loin dans un dialogue épisodique et passionnant qui se développe depuis dix années. Dans le jargon d'OSP, je suis ce qu'ils appellent un membre source. C'est une manière fort gentille de dire qu'on partage beaucoup de choses, essentiellement l'enthousiasme pour les ouvertures politiques et poétiques qu'offre le logiciel libre. Comme compagnon de route d'OSP, j'ai une connaissance de leur travail qui est à la fois intime et superficielle. Ce texte ne sera pas une introduction à OSP. Ils ont produit de très beaux textes qui invitent à découvrir leur pratique. À cela, il n'y a rien à ajouter. <sup>1</sup> On trouvera ici plutôt une série de questions qui émergent d'une discussion à bâtons rompus. On y parlera de graphisme, mais aussi de magie, de temps et de contes de fées. Ce texte est émaillé de termes techniques. J'ai essayé tant que possible de les commenter dans le flux du texte ou en notes car pour OSP la technique n'est jamais seulement de la technique mais toujours aussi autre chose.
	</p>
	
	<p>
		Ce texte est basé sur une conversation qui a lieu à Bruxelles. Nous avons rendez-vous à 18 heures. Alex et Stéphanie font des aller-retours dans la cuisine qui communique avec la pièce où nous nous tenons. Au fur et à mesure de la conversation, le repas se prépare. Dans la grande tradition « culinaire » d'OSP qui n'a pas choisi par hasard l'extension « .kitchen » pour son nom de domaine. La cuisine c'est tout un environnement de partage, l'importance du repas comme moment qui crée du collectif, c'est aussi le partage de la recette. La cuisine, c'est l'opposé de la salle-à-manger, c'est là où l'on est dans le faire plutôt que dans la consommation, dans le processus, dans le comment. Inviter quelqu'un dans la cuisine, c'est lui donner accès aux techniques, c'est une forme de transparence. On peut suivre l'évolution de l'aliment du cru au cuit, de sa matière brute à son raffinement progressif. La cuisine est un lieu de socialité, on fait en parlant.
		
		<img id="img-jam" src="http://ospublish.constantvzw.org/blog/wp-content/uploads/P1040643rr.jpg" />
	</p>
	
	<p>
		« Mais c'est quoi la magie ? » Cette question marque un arrêt dans une conversation qui coulait tranquillement. Tout à coup, il faut s'y reprendre à plusieurs fois pour être sûrs de se comprendre. On explore les significations multiples du mot. On crée des oppositions. Le bon magicien contre le mauvais prestidigitateur, celui qui fascine par ce qu'il fait contre celui qui se joue de l'attention du public. Je réécoute l'enregistrement de cette partie de la conversation plus attentivement. Tout commence par une remarque de Gijs qui compatit avec les étudiants qui suivent leurs ateliers et qui se plaignent de la difficulté d'utiliser des logiciels libres. Utiliser ces nouveaux outils leur semble rendre tout plus difficile. L'enjeu de ces ateliers est de leur faire accepter qu'il faut passer par cette sensation d'inconfort pour découvrir des choses nouvelles. Femke Snelting<sup>2</sup> a décrit ce moment dans son texte « Awkward Gestures »<sup>3</sup> au moment où elle-même découvrait ces logiciels. « Il faut apprendre à avancer à tâtons » insiste Stéphanie, c'est un apprentissage graduel. Garder un pied sur la terre ferme pour pouvoir poser l'autre dans l'inconnu, puis une fois celui-ci assuré, faire le prochain pas. « Il n'y a pas de magie ». Le mot est lâché. Sarah, en souriant : « mais il y a parfois du vaudou ». Gijs : « Et puis comprendre n'est-ce pas gâcher le plaisir ? » Chacun y met du sien pour démêler la question. S'y nouent la fascination pour quelque chose que le code produit et qui nous surprend et l'exigence de le comprendre. De ne pas « cacher comment cela marche ». Suspendre, se surprendre, cacher, dévoiler, expliquer. Une tension passe entre ces termes. Ce ne sont pas seulement des métaphores visuelles : montrer ou cacher, mais aussi des questions de temps. La magie doit aller vite. Comprendre prend du temps. Expliquer nécessite une « décomposition étape par étape ». Au contraire de la magie qui compresse, intensifie. Intensité contre séquence. 
	Peut-on sortir de cette opposition ?
	</p>
	
	<p>
		Je remonte le fil de la conversation. On a beaucoup parlé de temps. Des durées douces et des trépidations. Parler des temps du collectif amène chacun à parler de la mesure de son engagement. Le temps implique la finitude du corps. Un corps, on n'en a qu'un, il ne peut être qu'à un endroit à la fois. On a le temps ou on ne l'a pas. Il nous force à décider, s'engager. Il y a un athlétisme chez les OSP. Je ne parle pas ici des corps jeunes et beaux qui posent l'air de rien sur des photos à rendre des rock stars jalouses.<sup>4</sup> Je parle de la connaissance de l'effort et de son économie. Cette précision dans la parole quand ils décrivent les moments de dilatation et d'intensification. On parle trop souvent du graphisme comme d'une discipline de l’œil et trop peu comme d'une discipline du temps. Les graphistes sont soumis à une cadence de production extrêmement rapide. Et de par ses choix, OSP encore plus. Il leur faut conjuguer les durées longues et difficilement quantifiables des développements logiciels avec le couperet des deadlines. S'entrechoquent dans leur pratique les temps du tâtonnement, de l'erreur ou de l'impasse, du débogage, l'attente d'un <em>patch</em>, les surprises de la mise à jour, les accélérations foudroyantes de la ligne de commande. Le temps qui est donné par les outils et celui que les outils reprennent. Et à cela s'ajoute comme pour les autres graphistes, la lenteur des négociations, les séquences des plannings, des rétroplannings, les temps mous que l'on peut comprimer, la nuit, le sommeil.<sup>5</sup> Athlétisme. Il faut une connaissance de sa résistance personnelle et de celle du souffle collectif. L'enjeu est de faire exister la durée propre du collectif alors qu'un nombre infini d'horloges extérieures tentent de lui dicter son tempo, de saccader sa maturation.  
	</p>
	
	<p>
		Avec cette question : comment cette durée peut-elle durer ? Que se passe-t-il si le corps lâche ? Est-ce que cette durée est un temps plein ? Ou partiel ? A-t-elle un dehors ? Puis-je y entrer et en sortir ? Puis-je avoir un travail à&nbsp;côté ?
		
		<img id="img-etsi" src="/document/img/et-si.svg" />
	</p>
	
	<p>
		Les membres d'OSP ont beaucoup écrit sur leur manière de fonctionner. Le souci de transparence les pousse à une série d'expériences comme celle des OSP meets où le fonctionnement du collectif et les décisions de budget étaient discutées en public, et où toute personne intéressée était invitée à intervenir. Le <em>log</em> de leur Git,<sup>6</sup> journal de bord de leur outil de mise en commun du code, sorte de conciliabule infini, est inséré comme un long colophon en fin du programme qu'ils ont réalisé pour le théâtre la Balsamine.<sup>7</sup> Affirmer, revendiquer le processus de travail. Il y a presque de l’exhibitionnisme du processus. On retrouve le même log, mis à jour en temps réel, en page d'entrée de leur site. On  y est accueilli par le long fil de commentaires issus de chaque modification faite sur un fichier d'un projet en cours. On les entend à l’œuvre plus qu'on ne les lit. <em>« Alex Leray divulged — oups sorry… »</em> Accéder à leur portfolio demande plus d'effort. La porte principale mène toujours à la cuisine, on pourra passer au salon, si on veut vraiment. Souci de transparence extrême. Ou renversement copernicien ? Dans ce monde du design graphique où tout doit être immédiatement accessible et sans bavure, échanger les rôles du résultat et du processus déstabilise. Le résultat final est secondaire ? 
		Un épiphénomène du processus ? Ou alors est-ce une critique de la réification ? De la marchandisation du travail ? Il y a des pointes marxisantes dans certains textes d'OSP où l'on parle de dé-prolétarisation du travail…<sup>8</sup> Mais OSP est loin de la posture critique, l'engagement est dans le faire. Et même s'il prend beaucoup d'importance, le processus reste tendu vers le résultat. La quantité impressionnante de retouches visibles dans les commits<sup>9</sup> montrent bien qu'ils affinent le code avec beaucoup de soin afin d'obtenir le résultat escompté. L'emphase mise sur git à la fois archive en temps réel du processus et médiateur entre les graphistes et leur travail correspond à un credo maintes fois répété. La relation entre l'outil et la pratique est déterminante. <em>Tools shape practice, practice shapes tools.</em><sup>10</sup> Mais qu'en est-il des formes qui sont produites par cette relation ? Qu'en est-il des affects qui sont produits ? De comment ces créations graphiques nous touchent, nous émeuvent ? Qu'en est-il du sensible ? Comment ces outils-là et ces pratiques-là mènent à ces formes-là ? Le git est saturé de détails sur « comment c'est fait ». Mais quand les messages commencent à apparaître sur le site web, commentant dans le détail chaque changement, le résultat visuel semble déjà décidé, partagé de manière implicite entre les membres, ils savent déjà où ils vont.
	</p>
	
	<p>
		Parmi les designers contemporains, les OSP ne sont pas les seuls à donner une grande importance à l'outil et à chercher dans l'outil digital une réponse aux questions de communication visuelle contemporaine. Christophe Haag de Lafkon,<sup>11</sup> par exemple, publie lui aussi le code de ses créations, programme ses propres outils et diffuse le tout sous des licences libres. Si l'on observe le travail de Lafkon, la différence saute immédiatement aux yeux. Chez lui, la relation entre le processus technique et le résultat visuel est une causalité linéaire. Si l'on considère les règles de départ inscrites dans le programme et son résultat, la conclusion est inéluctable. La boucle de commande et contrôle se resserre autant sur le programme qui s'exécute que sur l'univers visuel qu'il produit. D'un travail à l'autre, on voit s'amplifier les conséquences esthétiques d'un même parti-pris programmatique. Chez OSP, il serait vain de chercher une quelconque linéarité. On va de surprise en surprise. On sent que si on prend au sérieux leur affirmation que l'outil a tant d'importance, il faudra accepter que cette relation est multiple et complexe. Ce qui est réjouissant. Mais ce qui est surprenant c'est qu'OSP si engagé à s'expliquer sur tout ce que l'on considère comme tabou depuis les réalités financières jusqu'au détail technique du moindre recoin du système d'exploitation, reste fort discret sur l'articulation qu'ils perçoivent entre les formes et les outils. Passée l'affirmation de principe, le discours devient allusif. Depuis que je suis le travail d'OSP, j'ai toujours été intrigué par ce moment où la volubilité du groupe s'arrête. Durant l'interview, je tente à plusieurs reprises d'aborder la question. Je vois un demi-sourire au coin d'une lèvre et aussi l'étonnement que cela n'aille pas de soi. On pourrait penser que c'est une zone d'impensé, inarticulée. Ou bien qu'il y a une pudeur à cet endroit. Ou trop d'évidence. Mais quelque chose invite à la prudence.
		Ce n'est pas parce qu'OSP est discret qu'il soit complètement silencieux sur la question. Je pense à ces déclarations fortes qui préfaçaient un atelier appelé Collision dans lequel était visé l'héritage oppressant de Gutenberg, la prison de la grille, et un appel à s'approprier l'outil informatique pour libérer une forme nouvelle du texte. Mais pas d'archive. Pas de référence ensuite. « Oui c'est Pierre. »<sup>12</sup> On minimise. On n'y reviendra pas. Ou une interview de Femke Snelting qui aborde le sujet du design génératif mais qui désamorce au préalable « je marche sur des œufs ici ».<sup>13</sup> Le discours formel d'OSP ressemble à cette taupinière ponctuée de trous d'où jaillissent des questions et des élans. Un discours interrompu où ces trous fonctionnent comme des points de suspension. Exit l'intelligibilité, la décomposition systématique et militante. On retrouve la magie et peut-être plus encore la prestidigitation. L'art de jouer avec l'attention, ce que l'on indique clairement d'une main pour le subtiliser de l'autre.
		
		<img id="img-collision" src="/document/img/co-position-covers.png" />
	</p>
	
	<p>
		J'essaie alors de faire un pas de côté. Une performance d'OSP me revient en mémoire. De retour de leur premier LGM,<sup>14</sup> l'équipe revient de Montréal nourrie de la rencontre avec les développeurs de leurs outils. OSP organise une fête pour leurs amis bruxellois. Le moment crucial de cet événement est la démonstration d'un script écrit pour le logiciel Scribus qui transforme la grenouille, en un prince. Et puis ce prince, en grenouille. Cette <em>print party</em> donnera naissance au logo OSP. On peut comprendre cette image comme ceci : le logiciel graphique libre en 2016 est une créature en devenir qui attend le baiser du prince pour s'épanouir. Mais ce n'est pas un aller simple. Il y a aller-retour entre les deux pôles. Apprendre à maîtriser l'outil et désapprendre sont aussi importants l'un que l'autre. Mais c'est aussi une relation particulière à la visibilité, devenir un prince, c'est se mettre en pleine lumière.
		Revenons-en à présent à notre question de la forme. Pour approcher cette question, j'aimerais utiliser le même procédé imagé et proposer moi aussi une translation entre deux figures. Je disais que leur discours m'évoquait une taupinière. Imaginons à présent que la grenouille mette de côté un instant son destin princier. Et, lassée de tant de visibilité, elle soit prise d'un devenir-taupe. Imaginons cette fois la transition de la grenouille à la taupe. Et de la taupe à la grenouille. Imaginons des aller-retours souterrains de la cuisine au terrier. Qu'on suive la taupe dans son activité de creusage frénétique dans le labyrinthe. Mais comment suivre la taupe ? On est toujours en retard sur elle. À peine voit-on un trou qu'elle est déjà ailleurs à creuser. Elle déjoue les attentes.
	</p>
	
	<p>
		En écrivant ces lignes je suis loin de critiquer ou de condamner. D'exiger une illusoire clarté complète, une élucidation intégrale. D'appeler à un programme sans ambiguïté ni esquive. J'aime la taupe. Myope, soupçonneuse de trop de clarté, sait-elle où elle va ? Elle est peut-être tout l'enjeu. Laisser l'animal creuser sans la contraindre à rester dans une direction. Si la taupe ne change pas de cap, elle est morte. Alors j'essaie de voir en même temps l'explicitation du code, le détail maniaque du processus ET ce langage énigmatique, en pointillé pour parler de la forme. L'affirmation ET l'esquive. La transparence des outils et de la pratique devient alors aussi une technique de prestidigitateur pour laisser la forme errer, la dessaisir du corset d'un art director, d'un style ou d'une image de marque. C'est peut-être parce qu'ils craignent plus l'emprise du discours sur la forme que celle des outils qu'ils parlent tant de ces derniers. Dévier le discours de la forme sur la technique permet peut-être plus de liberté pour la forme finalement. Elle devient libre d'embrasser la dichotomie, de rechercher, myope, sa direction et d'en changer. Ou mieux que dévier, l'enrouler, l'encrypter, l'entrelacer.
		<em>Tools shape practices. Practices shape tools.</em> Certainement. Mais alors aussi : la grenouille fait la taupe et la taupe, la grenouille.
		
		<img id="img-lgru" src="/document/img/shapes-shape.svg">
	</p>
	
	
	<ol id="footnotes" class="h2p-region-break">
		<li><sup>1</sup> Pour ceux qui les découvriraient à travers ce texte, je suggère de se rapporter à la page info de leur site web qui résume les éléments nécessaires pour comprendre les principes sur lesquels se basent la caravane OSP ainsi qu'une série de liens vers les textes originaux. Voir <a>http://osp.kitchen/</a>.</li>
		
		<li><sup>2</sup> Femke Snelting, designer, artiste et activiste, est une des initiatrices d'Open Source Publishing.</li>
		
		<li><sup>3</sup> « Awkward gestures : designing with Free Software », Femke Snelting, <a>http://ospublish.constantvzw.org/blog/wp-content/uploads/awkward_gestures</a>.</li>
		
		<li><sup>4</sup> <img id="img-wtc" src="/document/img/osp-at-wtc.jpg">.</li>
		
		<li><sup>5</sup> Et… Congés scolaires, examens, horaire de la crèche, rendez-vous médicaux. Ce à quoi ils tentent de répondre en incluant dans leurs contrats du temps pour la recherche, du temps pour leur horloge intérieure.</li>
		
		<li><sup>6</sup> Un système de version qui stocke tous les fichiers produits pour un travail et l’historique du projet. OSP donne accès à tous ces fichiers à leurs clients ainsi qu'au public.</li>
		
		<li><sup>7</sup> Théâtre bruxellois avec lequel le groupe a noué une relation de travail intime. Voir <a>http://balsamine.be/</a>.</li>
		
		<li><sup>8</sup> Une citation du manifeste d'Ars Industrialis dans le texte d'introduction de Relearn : « Le caractère exemplaire des combats menés par les acteurs du logiciel libre—trouve-t-on dans le manifeste d’Ars Industrialis—tient à ce que pour la première fois, des travailleurs issus du monde industriel inventent une organisation nouvelle du travail et de l’économie qui a fait de la déprolétarisation son principe et son credo. » Voir <a>http://f-u-t-u-r-e.org/r/02_OSP_Relearn_FR.md</a>.</li>
		
		<li><sup>9</sup> Acte d'ajouter un changement à l'historique du projet.</li>
		
		<li><sup>10</sup> Slogan du Libre Graphics Research Unit organisé par l'association Constant, dans lequel OSP a pris une part importante. Voir <a>http://lgru.net/</a>.</li>
		
		<li><sup>11</sup> Voir son travail sur <a>http://lafkon.net/</a>.</li>
		
		<li><sup>12</sup> Pierre Huyghebaert, membre d'OSP et fondateur de Speculoos, centre de spécialités graphiques. Voir<a>http://www.speculoos.com</a>.</li>
		
		<li><sup>13</sup> Ma traduction pour le très beau « I feel a bit on thin ice » Voir <a>http://spc.org/fuller/interviews/open-source-publishing-interview-with-femke-snelting/</a>.</li>
		
		<li><sup>14</sup> Libre Graphics Meeting, rencontre annuelle des développeurs et utilisateurs de logiciels libres et open source consacrés aux arts visuels. Voir <a>http://libregraphicsmeeting.org</a>.</li>
	</ol>
	
</div>


<div id="bioNicolas">
<p>
<strong>Nicolas Malevé</strong> (Bruxelles, 1969) est un programmeur auto-didacte et un
artiste qui s'intéresse à la poétique et la politique des archives.
En plus d'être membre actif de l'association Constant pour
les arts et media dont le fil conducteur est la culture libre.
 Il est actuellement doctorant à South Bank University à Londres où il travaille
sur une thèse à propos des relations entre les algorithmes et les training
data dans le monde de l'image en réseau.
</p>
</div>


<div id="members" class="">
	<h1>Mais qui est OSP  ?</h1>
	
	
	<svg viewBox="0 0 60 60" id="frog">
	    
	<svg   xmlns:dc="http://purl.org/dc/elements/1.1/"   xmlns:cc="http://creativecommons.org/ns#"   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"   xmlns:svg="http://www.w3.org/2000/svg"   xmlns="http://www.w3.org/2000/svg"   xmlns:sodipodi="http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd"   xmlns:inkscape="http://www.inkscape.org/namespaces/inkscape"   width="59"   height="67"   id="svg5365"   sodipodi:version="0.32"   inkscape:version="0.48.2 r9819"   version="1.0"   sodipodi:docname="OSP_new-frog.svg"   inkscape:output_extension="org.inkscape.output.svg.inkscape"   inkscape:export-filename="/home/ludi/wrk/osp/osp.web.themes/layouts/pict/OSP_new-frog.png"   inkscape:export-xdpi="72"   inkscape:export-ydpi="72">  <defs     id="defs5367" />  <sodipodi:namedview     id="base"     pagecolor="#ffffff"     bordercolor="#666666"     borderopacity="1.0"     gridtolerance="10000"     guidetolerance="10"     objecttolerance="10"     inkscape:pageopacity="0.0"     inkscape:pageshadow="2"     inkscape:zoom="4.6090747"     inkscape:cx="3.0407093"     inkscape:cy="48.329072"     inkscape:document-units="px"     inkscape:current-layer="layer1"     showgrid="false"     units="px"     inkscape:window-width="1280"     inkscape:window-height="739"     inkscape:window-x="0"     inkscape:window-y="26"     borderlayer="true"     inkscape:showpageshadow="false"     showguides="true"     inkscape:guide-bbox="true"     inkscape:window-maximized="1"     width="59px" />  <metadata     id="metadata5370">    <rdf:RDF>      <cc:Work         rdf:about="">        <dc:format>image/svg+xml</dc:format>        <dc:type           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />        <dc:title />      </cc:Work>    </rdf:RDF>  </metadata>  <g     inkscape:label="Layer 1"     inkscape:groupmode="layer"     id="layer1"     transform="translate(0,-358.19771)">    <path       style="fill:none;stroke:#ffaa00;stroke-width:0.75;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit
:4;stroke-opacity:1;stroke-dasharray:none;display:inline;enable-background:new"       d="M 51.00118,388.92502 36.963607,375.30444 6.2478517,402.96255 26.122769,414.08139 26.26175,405.18632 47.804487,412.9695 37.38057,402.54559 z"       id="path4397"       inkscape:connector-curvature="0" />    <path       style="fill:none;stroke:#ffaa00;stroke-width:0.75;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-opacity:1;stroke-dasharray:none;display:inline;enable-background:new"       d="m 44.885821,366.13139 -0.694946,14.73247 -14.315502,0 0,-15.01043 4.030576,9.45101 1.945792,-9.17305 4.308558,9.45102 z"       id="path4399"       inkscape:connector-curvature="0" />  </g></svg>
	</svg>
	
	
	
	<div>
		<h2 id="effectivemembers">Membres<br/>effectifs</h2>
		
		<ul>
			<li>Gijs de Heij</li>
			<li>Pierre Huyghebaert</li>
			<li>Alexandre Leray</li>
			<li>Ludivine Loiseau</li>
			<li>Sarah Magnan</li>
			<li>Colm O’Neill</li>
			<li>Eric Schrijver</li>
			<li>Stéphanie Vilayphiou</li>
		</ul>
	</div>
	
	
	<div>
		<h2 id="sourcemembers">Membres<br/>source</h2>
		
		<ul>
			<li>Harrisson</li>
			<li>Nicolas Malevé</li>
			<li>Pierre Marchand</li>
			<li>John Haltiwanger</li>
		</ul>
	</div>
	
	<div>
		<h2 id="adherentmembers">Membres<br/>adhérents</h2>
		
		
		
		<ul>
			<li><h3 id="branchmembers">Membres<br/>branchés</h3></li>
			<li>Thomas Buxó</li>
			<li><h3 id="observatorymembers">Membres<br/>observateurs</h3></li>
			<li>Fabien Dehasseler</li>
			<li>Greg Nijs</li>
		</ul>
	</div>
	
	<div>
		<h2 id="board">Conseil<br/>d'administration</h3>
		
		<ul>
			<li>Femke Snelting</li>
			<li>Catherine Lenoble</li>
			<li>Bram Crevits</li>
			<li>Nik Gaffney</li>
			<li>Maxime Lambrecht</li>
		</ul>
	</div>
</div>


<div id="colophon">
<h1>Colophon</h1>
Éléments graphiques et de mises en pages tirées de :

<dl>
	<dt>Médor</dt><dd>micro-colonne et titrage</dd>
	<dt>Blijven Kijken</dt><dd>colonnes à largeur multiple</dd>
	<dt>Balsamine 2015–2016</dt><dd>trame de fond</dd>
	<dt>Relearn</dt><dd>fonte Meta Herschey Times</dd>
	<dt>Balsamine 2014–2015</dt><dd>fontes Ume Plume Gothic 220 et Ume Plume Mincho 220</dd>
	<dt>le75.be</dt><dd>zigzag</dd>
</dl>

Mis en page avec html2print, sur un navigateur QTwebkit maison (merci à François Zajega pour son aide !). Voir <a>http://osp.kitchen/tools/html2print/</a> et <
</div>

