#! /usr/bin/env bash

calc () {
    bc -l <<< "$@"
}

clear

echo POTRACE | toilet -f block -F border --gay

sleep 2

cat <<DELIM
Potrace is an open-source, cross-platform computer program which converts bitmapped images into vector graphics. It is written and maintained by Peter Selinger.
DELIM

sleep 2

cat <<DELIM
Durant nos dix années d'existance, nous avons eu l'occasion d'utiliser Potrace dans de nombreux projets. parmis ceux-ci:
* fonzie
** description
* fonz
** description
* dingbat liberation fest
** description
* Seoul Font Karaoke
* ...
DELIM

sleep 2

cat <<DELIM
Since the first time I’ve used an autotrace program -Adobe Streamline 1.0 in the early nineties- I’ve been disappointed by the unavoidable angles in curves, named kinks or cusps, that pledged the vector output. Lots of designers and developers seem not to care about it, but for me it is simply the difference between artificial shapes that scream “I’m a vector!” and natural shapes where every sharp edge is a small curve when you look really close. That kind of ultra detail may seem useless and/or nerdy, but it can really make the difference in typography. Like in my work where I use bitmaps as sources.
DELIM

sleep 10

clear

#for i in $(seq 0 1 133); do
    #val=$(calc $i / 100)
    #echo $val

    #potrace \
        #-a $(calc $i / 100) \
        #--flat \
        #-W "400pt" \
        #-s \
        #-o - \
        #specimens/univers_else_023_extract.bmp | \
    #python \
        #bin/reveal_svg.py \
        #- \
        #out/$(printf '%03d' $i).svg

    ##inkscape  out/$(printf '%03d' $i).svg --export-png=out/$(printf '%03d' $i).png -w1280

    #display out/$(printf '%03d' $i).svg&
#done

#convert out/*.png out/final.gif

#############################################
# alphamax 0-0.2-0.4-0.6-0.8-1-1.2-1.3-1.34 #
#############################################

echo "ALPHAMAX 1.334 (OR THE LIMIT BETWEEN ARTIFICIAL AND NATURAL)" | toilet -f block -F border --gay

sleep 10

cat <<DELIM
POTRACE –ALPHAMAX 1.334 (OR THE LIMIT BETWEEN ARTIFICIAL AND NATURAL)
DELIM

alphamax=(0 0.2 0.4 0.6 0.8 1 1.2 1.3 1.34)
j=0

for i in "${alphamax[@]}"
do
    potrace \
        -a $i \
        --flat \
        -W "400pt" \
        -s \
        -o - \
        specimens/univers_else_023_extract.bmp | \
    python \
        bin/reveal_svg.py \
        - \
        out/01_alphamax_$i.svg

    #inkscape  out/$(printf '%03d' $i).svg --export-png=out/$(printf '%03d' $i).png -w1280

    echo
    echo

    display -geometry +$((0 + 80 * j))+$((0 + 80 * j)) out/01_alphamax_$i.svg&
    j=$((j + 1))
done

sleep 2
pkill display

#########################
# specles 0-10-100-1000 #
#########################

#speckles=(0 10 100 1000)

#for i in "${speckles[@]}"
#do
    #potrace \
        #-t $i \
        #--flat \
        #-W "400pt" \
        #-s \
        #-o - \
        #specimens/univers_else_023_extract.bmp | \
    #python \
        #bin/reveal_svg.py \
        #- \
        #out/01_despeckles_$i.svg

    ##inkscape  out/$(printf '%03d' $i).svg --export-png=out/$(printf '%03d' $i).png -w1280

    ##display out/01_despeckles_$i.svg&
#done

########################
# optimize 0-1-2-3-4-5 #
########################

#optimize=(0 1 2 3 4 5)

#for i in "${optimize[@]}"
#do
    #potrace \
        #-t $i \
        #--flat \
        #-W "400pt" \
        #-s \
        #-o - \
        #specimens/univers_else_023_extract.bmp | \
    #python \
        #bin/reveal_svg.py \
        #- \
        #out/01_optimize_$i.svg

    ##inkscape  out/$(printf '%03d' $i).svg --export-png=out/$(printf '%03d' $i).png -w1280

    ##display out/01_optimize_$i.svg&
#done

#############################################################
# mais surtout aussi résolution 10-30-100-300-1000-3000 dpi #
#############################################################

#resolution=(10 30 100 300 1000 3000)

#for i in "${resolution[@]}"
#do
    #potrace \
        #-r $ix$i \
        #--flat \
        #-W "400pt" \
        #-s \
        #-o - \
        #specimens/univers_else_023_extract.bmp | \
    #python \
        #bin/reveal_svg.py \
        #- \
        #out/01_resolution_$i.svg

    #inkscape  out/$(printf '%03d' $i).svg --export-png=out/$(printf '%03d' $i).png -w1280

    #display out/01_resolution_$i.svg&
#done

