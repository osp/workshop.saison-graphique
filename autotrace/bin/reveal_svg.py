#! /usr/bin/env python2
# -*- coding: utf-8 -*-


# reveal_svg.py: a script to reveal the svg construction

# Copyright (C) 2016 Open Source Publishing (Alexandre Leray)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


from xml.dom import minidom
from svg.path import parse_path, CubicBezier

def main(src):
    doc = minidom.parseString(src)  # parseString also exists
    svg = doc.firstChild

    for path in doc.getElementsByTagName('path'):
        d = path.getAttribute('d')
        path.setAttribute("fill", "none")
        path.setAttribute("stroke", "black")
        path.setAttribute("stroke-width", "3")

        for i in parse_path(d):
            if isinstance(i, CubicBezier):
                node = doc.createElement("line")
                node.setAttribute("x1", "%s" % i.control1.real)
                node.setAttribute("y1", "%s" % i.control1.imag)
                node.setAttribute("x2", "%s" % i.start.real)
                node.setAttribute("y2", "%s" % i.start.imag)
                node.setAttribute("stroke", "blue")
                node.setAttribute("stroke-width", "2")
                path.parentNode.appendChild(node)

                node = doc.createElement("line")
                node.setAttribute("x1", "%s" % i.control2.real)
                node.setAttribute("y1", "%s" % i.control2.imag)
                node.setAttribute("x2", "%s" % i.end.real)
                node.setAttribute("y2", "%s" % i.end.imag)
                node.setAttribute("stroke", "blue")
                node.setAttribute("stroke-width", "2")
                path.parentNode.appendChild(node)

                node = doc.createElement("circle")
                node.setAttribute("cx", "%s" % i.control1.real)
                node.setAttribute("cy", "%s" % i.control1.imag)
                node.setAttribute("r", "4")
                node.setAttribute("fill", "white")
                node.setAttribute("stroke", "blue")
                node.setAttribute("stroke-width", "2")
                path.parentNode.appendChild(node)

                node = doc.createElement("circle")
                node.setAttribute("cx", "%s" % i.control2.real)
                node.setAttribute("cy", "%s" % i.control2.imag)
                node.setAttribute("r", "4")
                node.setAttribute("fill", "white")
                node.setAttribute("stroke", "blue")
                node.setAttribute("stroke-width", "2")
                path.parentNode.appendChild(node)

            node = doc.createElement("circle")
            node.setAttribute("cx", "%s" % i.start.real)
            node.setAttribute("cy", "%s" % i.start.imag)
            node.setAttribute("r", "8")
            node.setAttribute("fill", "white")
            node.setAttribute("stroke", "red")
            node.setAttribute("stroke-width", "2")
            path.parentNode.appendChild(node)

            node = doc.createElement("circle")
            node.setAttribute("cx", "%s" % i.end.real)
            node.setAttribute("cy", "%s" % i.end.imag)
            node.setAttribute("r", "8")
            node.setAttribute("fill", "white")
            node.setAttribute("stroke", "red")
            node.setAttribute("stroke-width", "2")
            path.parentNode.appendChild(node)


    ret = doc.toprettyxml()
    doc.unlink()
    return ret


if __name__ == '__main__':
    import argparse
    import sys
    parser = argparse.ArgumentParser(description='Finds bigrams (word couples) in a text')
    parser.add_argument('infile',  nargs='?', type=argparse.FileType('r'), default=sys.stdin,
                        help="the source `.txt` file")
    parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout,
                                help="the file to save the result to (stdout is used if not given)")

    args = parser.parse_args()

    content = args.infile.read()
    out = main(content)
    args.outfile.write(out.encode("utf-8"))
